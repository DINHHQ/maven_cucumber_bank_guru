package commons;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.Reporter;

public class AbstractTest {

	WebDriver driver;
	protected final Log log;
	private final String workingDir = System.getProperty("user.dir");

	public AbstractTest() {
		log = LogFactory.getLog(getClass());
	}

	public WebDriver openMultiBrowser(String browserName, String browserVersion, String url) {

		if (browserName.equals("chrome")) {
			// System.setProperty("webdriver.chrome.driver",
			// ".\\resources\\chromedriver.exe");
			ChromeDriverManager.getInstance().version(browserVersion).setup();
			driver = new ChromeDriver();
		} else if (browserName.equals("firefox")) {
			FirefoxDriverManager.getInstance().version(browserVersion).setup();
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, workingDir + "\\FirefoxLog.txt");
//			FirefoxOptions options = new FirefoxOptions();
//			driver = new FirefoxDriver(options);
		}else if (browserName.equals("chrome_headless")) {
			ChromeDriverManager.getInstance().version(browserVersion).setup();
//			ChromeOptions options = new ChromeOptions();
//			options.addArguments("headless");
//			options.addArguments("window-size=1920x1080");
//			driver = new ChromeDriver(options);
		} else if (browserName.equals("ie11")) {
			InternetExplorerDriverManager.getInstance().version(browserVersion).setup();
			driver = new InternetExplorerDriver();
			
		} else if (browserName.equals("firefox_headless")) {
			FirefoxDriverManager.getInstance().version(browserVersion).setup();
			FirefoxBinary firefoxBinary = new FirefoxBinary();
			firefoxBinary.addCommandLineOptions("--headless");
//			FirefoxOptions firefoxOptions= new FirefoxOptions();
//			firefoxOptions.setBinary(firefoxBinary);
//			driver = new FirefoxDriver(firefoxOptions);
		}
		else {
			System.out.println("Can't find browser");
		}

		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}

	public int randomData() {
		Random ran = new Random();
		int random = ran.nextInt(9999);
		return random;

	}

	private boolean checkPassed(boolean condition) {
		boolean pass = true;
		try {
			log.info("============ Pass ============= ");
			Assert.assertTrue(condition);
		} catch (Throwable e) {
			log.info("============ Fail ============= ");
			pass = false;
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;

	}

	public boolean verifyTrue(boolean condition) {
		return checkPassed(condition);

	}

	public boolean checkFailed(boolean condition) {
		boolean pass = true;
		try {
			log.info("============ Fail ============= ");
			Assert.assertFalse(condition);
		} catch (Throwable e) {
			log.info("============ Pass ============= ");
			pass = false;
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;

	}

	public boolean verifyFailed(boolean condition) {
		return checkFailed(condition);

	}

	public boolean checkEquals(Object actual, Object expected) {
		boolean pass = true;
		try {
			log.info("============ Pass ============= ");
			Assert.assertEquals(actual, expected);
		} catch (Throwable e) {
			log.info("============ Fail ============= ");
			pass = false;
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	public boolean verifyEquals(Object actual, Object expected) {
		return checkEquals(actual, expected);
	}

	protected void closeBrowser(WebDriver driver) {
		try {
			// Detect OS (Windows/ Linux/ MAC)
			String osName = System.getProperty("os.name").toLowerCase();
			String cmd = "";
			driver.quit();
			if (driver.toString().toLowerCase().contains("chrome")) {
				// Kill process
				if (osName.toLowerCase().contains("mac")) {
					cmd = "pkill chromedriver";
				} else {
					cmd = "taskkill /F /FI \"IMAGENAME eq chromedriver*\"";
				}
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}
			if (driver.toString().toLowerCase().contains("internetexplorer")) {
				cmd = "taskkill /F /FI \"IMAGENAME eq IEDriverServer*\"";
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}
			// log.info("---------- QUIT BROWSER SUCCESS ----------");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	

}
