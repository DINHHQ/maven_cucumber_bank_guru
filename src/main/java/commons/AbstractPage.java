package commons;

import interfaces.AbstractPageUI;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BalanceEnquiryPageObject;
import pageObjects.DeleteAccountPageObject;
import pageObjects.DeleteCustomerPageObject;
import pageObjects.DepositPageObject;
import pageObjects.EditCustomerPageObject;
import pageObjects.FundTransInputPageObject;
import pageObjects.HomePageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.PageManagerDriver;
import pageObjects.WithdrawalPageObject;

public class AbstractPage {

	public void openAnyURL(WebDriver driver, String URL) {
		driver.get(URL);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public String getTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String getCurrentURL(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public void back(WebDriver driver) {
		driver.navigate().back();
	}

	public void forward(WebDriver driver) {
		driver.navigate().forward();
	}

	public void refresh(WebDriver driver) {
		driver.navigate().refresh();
	}

	public void clickToElement(WebDriver driver, String locator, String... value) {
		locator = String.format(locator,(Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void sendkeyToElement(WebDriver driver, String locator, String inputValue, String...value) {
		locator = String.format(locator, (Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.clear();
		element.sendKeys(inputValue);
	}
	
	public void sendkeyToElementDate(WebDriver driver, String locator, String inputValue, String...value) {
		locator = String.format(locator, (Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(inputValue);
	}

	public void selectItemInDropdown(WebDriver driver, String locator, String value) {
		WebElement element = driver.findElement(By.xpath(locator));
		Select select = new Select(element);
		select.selectByValue(value);
	}

	public String getFirstItemInDropdown(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Select select = new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	public String getAttributeValue(WebDriver driver, String locator, String attribute) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attribute);
	}

	public String getTextElement(WebDriver driver, String locator, String...value) {
		locator = String.format(locator, (Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getText();
	}

	public int getSizeElement(WebDriver driver, String locator) {
		List<WebElement> elements = driver.findElements(By.xpath(locator));
		return elements.size();
	}

	public void checkTheCheckbox(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (!element.isSelected()) {
			element.click();
		}
	}

	public void uncheckTheCheckbox(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (element.isSelected()) {
			element.click();
		}
	}

	public boolean isControlDisplayed(WebDriver driver, String locator,String...value) {
		locator = String.format(locator,(Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isDisplayed();
	}

	public boolean isControlSelected(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isSelected();
	}

	public boolean isControlEnable(WebDriver driver, String locator, String... value) {
		locator = String.format(locator,(Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isEnabled();
	}

	public void acceptAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void cancelAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public String getTextAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		return alert.getText();
	}

	public void sendkeyAlert(WebDriver driver, String value) {
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(value);
	}

	public void switchToChildWindow(WebDriver driver, String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}

	}

	public void switchToWindowByTitle(WebDriver driver, String title) {
		// Get all windows ID
		Set<String> allWindows = driver.getWindowHandles();

		// Duyet qua tung ID
		for (String runWindows : allWindows) {
			// Switch qa tung ID
			driver.switchTo().window(runWindows);

			// Get title cua page do ra
			String currentTitle = driver.getTitle();

			// Title current windows = title truyen vao
			if (currentTitle.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(WebDriver driver, String parentWindow) {
		// Get all windows ID
		Set<String> allWindows = driver.getWindowHandles();

		// Duyet qua tung ID
		for (String runWindows : allWindows) {

			// Neu ID # parent ID
			if (!runWindows.equals(parentWindow)) {
				// Switch qa id do
				driver.switchTo().window(runWindows);

				// dong ID
				driver.close();
			}
		}
		// Switch qa parent Windows ID
		driver.switchTo().window(parentWindow);
		// Kiem tra no chi con lai 1 window (parent)
		if (driver.getWindowHandles().size() == 1)
			// return lai gia tri cho ham closeAllWithoutParentWindows (nhan gia tri tra ve
			// la boolean)
			return true;
		else
			// return lai gia tri cho ham closeAllWithoutParentWindows (nhan gia tri tra ve
			// la boolean)
			return false;
	}

	public void doubleClickToElement(WebDriver driver, String locator) {
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath(locator));
		action.doubleClick(element).perform();
	}

	public void hoverMouseToElement(WebDriver driver, String locator) {
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath(locator));
		action.moveToElement(element).perform();
	}

	public void rightClick(WebDriver driver, String locator) {
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath(locator));
		action.contextClick(element).perform();
	}

	public void drapAndDrop(WebDriver driver, String locatorDrap, String locatorDrop) {
		Actions action = new Actions(driver);
		WebElement elementDrap = driver.findElement(By.xpath(locatorDrap));
		WebElement elementDrop = driver.findElement(By.xpath(locatorDrop));
		action.dragAndDrop(elementDrap, elementDrop).perform();
	}

	public void keyPress(WebDriver driver, String locator) {
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath(locator));
		action.clickAndHold(element).perform();
	}

	public void sendKey(WebDriver driver, String locator, String filePath) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(filePath);
	}

	public void uploadRobot() throws Exception {

		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}
	
	public void waitForControlPresence(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
	public void waitForControlVisible(WebDriver driver, String locator, String...value) {
		locator = String.format(locator,(Object[])value);
		WebElement element = driver.findElement(By.xpath(locator));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void waitForControlNotVisible(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}
	
	public void waitForControlClickable(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void waitForAlertPresence(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.alertIsPresent());
	}
	
	public NewCustomerPageObject openNewCustomerPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"New Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"New Customer");
		return PageManagerDriver.getNewCustomerPageObject(driver);
	}
	
	public EditCustomerPageObject openEditCustomerPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Edit Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Edit Customer");
		return PageManagerDriver.getEditCustomerPageObject(driver);
	}
	
	public NewAccountPageObject openNewAccountPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"New Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"New Account");
		return PageManagerDriver.getNewAccountPage(driver);
	}
	
	public HomePageObject openHomePage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Manager");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Manager");
		return PageManagerDriver.gethomePageObject(driver);
	}
	
	public DepositPageObject openDepositPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Deposit");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Deposit");
		return PageManagerDriver.getDepositPageObject(driver);
	}
	
	public WithdrawalPageObject openWithdrawalPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Withdrawal");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Withdrawal");
		return PageManagerDriver.getWithdrawalPageObject(driver);
	}
	
	public FundTransInputPageObject openFundTransInputPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Fund Transfer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Fund Transfer");
		return PageManagerDriver.getFundTransInputPageObject(driver);
	}
	
	public BalanceEnquiryPageObject openBalanceEnquiryPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Balance Enquiry");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Balance Enquiry");
		return PageManagerDriver.getBalanceEnquiryPageObject(driver);
	}
	
	
	public DeleteAccountPageObject openDeleteAccountPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Delete Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Delete Account");
		return PageManagerDriver.getDeleteAccountPageObject(driver);
	}
	
	public DeleteCustomerPageObject openDeleteCustomerPage (WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS,"Delete Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS,"Delete Customer");
		return PageManagerDriver.getDeleteCustomerPageObject(driver);
	}
	
	
	public Object removeAttributeInDOM(WebDriver driver, WebElement element , String attribute,String...value) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);

		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			return null;
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
