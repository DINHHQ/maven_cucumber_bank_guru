package interfaces;

public class LoginPageUI {
	
	public static final String HERE_LINK="//a[text()='here']";
	public static final String USERID_TEXTBOX="//input[@name='uid']";
	public static final String PASSWORD_TEXTBOX="//input[@name='password']";
	public static final String LOGIN_BTN="//input[@name='btnLogin']";


}
