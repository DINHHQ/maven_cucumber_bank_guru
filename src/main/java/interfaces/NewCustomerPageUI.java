package interfaces;

public class NewCustomerPageUI {

	public static final String HOME_PAGE_LINK = "//a[text()='Manager']";
	public static final String EDIT_CUSTOMER_LINK = "//a[text()='New Customer']";
	public static final String CUSTOMER_GENDER = "//input[@name='rad1' and @value='f']";
	public static final String CUSTOMER_ADDR = "//textarea[@name='addr']";
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String CUSTOMER_MESSAGE_EDIT_SUCCESS = "//p[text()='Customer Registered Successfully!!!']";
	public static final String CUSTOMER_ID = "//td[text()='Customer ID']/following-sibling::td";
	public static final String CUSTOMER_DATE_OF_DATE = "//input[@name='dob']";
	
	

}
