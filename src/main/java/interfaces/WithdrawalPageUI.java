package interfaces;

public class WithdrawalPageUI {
	
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String CUSTOMER_MESSAGE_WITHDRAWAL_SUCCESS = "//p[contains(text(),'Transaction details of Withdrawal for Account %s')]";
	public static final String CUSTOMER_CURRENT_AMOUNT = "//td[text()='Current Balance']/following-sibling::td";

}
