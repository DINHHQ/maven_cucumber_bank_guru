package interfaces;

public class EditCustomerPageUI {

	public static final String NEW_ACCOUNT_LINK = "//a[text()='New Account']";
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String CUSTOMER_GENDER = "//input[@name='rad1' and @value='f']";
	public static final String CUSTOMER_ADDR = "//textarea[@name='addr']";
	public static final String CUSTOMER_MESSAGE_EDIT_SUCCESS = "//p[text()='Customer details updated Successfully!!!']";
	
	public static final String CUSTOMER_ID_TXT = "//input[@name='cusid']";

}
