package interfaces;

public class AbstractPageUI {

	public static final String DYNAMIC_LINKS = "//a[text()='%s']";
	public static final String DYNAMIC_PAGE_TITLE_MESSAGE="//p[@class='heading3' and text()='%s'] ";
	public static final String DYNAMIC_TEXTBOX ="//input[@name='%s']";
	public static final String DYNAMIC_TEXTAREA ="//textarea[@name='%s']";
	public static final String DYNAMIC_RADIO_BUTTON ="//input[@value='%s']";
	public static final String DYNAMIC_BUTTON ="//input[@name='%s']";
	public static final String DYNAMIC_SUCCESS_MESSAGE ="//p[@class='heading3' and text()='%s']";
	public static final String DYNAMIC_LABEL ="//td[text()='%s']/following-sibling::td";
	
	public static final String CUSTOMER_DYNAMIC ="";
	public static final String DYNAMIC_TEXT ="";
	


}
