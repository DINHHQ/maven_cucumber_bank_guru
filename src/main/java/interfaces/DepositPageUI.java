package interfaces;

public class DepositPageUI {
	
	public static final String LABEL_RESULT="//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]";
	public static final String NEW_CUSTOMER_LINK = "//a[text()='New Customer']";
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String ACCOUNT_ID = "//td[text()='Account ID']/following-sibling::td";
//	public static final String CUSTOMER_MESSAGE_DEPOSIT_SUCCESS = "//p[contains(text(),'Transaction details of Deposit for Account %s')]";
	public static final String CUSTOMER_MESSAGE_DEPOSIT_SUCCESS = "//p[text()='Transaction details of Deposit for Account %s']";
	public static final String CUSTOMER_CURRENT_AMOUNT = "//td[text()='Current Balance']/following-sibling::td";
	

}
