package interfaces;

public class NewAccountPageUI {
	public static final String DDL_ACCOUNT = "//select[@name='selaccount']";
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String CUSTOMER_MESSAGE_ACCOUNT_GENERATED_SUCCESS = "//p[text()='Account Generated Successfully!!!']";
	public static final String CUSTOMER_CURRENT_AMOUNT = "//td[text()='Current Amount']/following-sibling::td";
	public static final String ACCOUNT_ID = "//td[text()='Account ID']/following-sibling::td";



}
