package interfaces;

public class FundTransInputPageUI {
	
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String CUSTOMER_MESSAGE_FUNDTRANS_SUCCESS = "//p[contains(text(),'Fund Transfer Details')]";
	public static final String CUSTOMER_CURRENT_AMOUNT = "//td[text()='Amount']/following-sibling::td";
	

}
