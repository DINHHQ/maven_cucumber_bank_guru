package interfaces;

public class BalanceEnquiryPageUI {
	
	public static final String CUSTOMER_SUBMIT_BTN = "//input[@type='submit']";
	public static final String CUSTOMER_MESSAGE_BALANCE_SUCCESS = "//p[contains(text(),'Balance Details for Account %s')]";
	public static final String BALANCE = "//td[text()='Balance']/following-sibling::td";

}
