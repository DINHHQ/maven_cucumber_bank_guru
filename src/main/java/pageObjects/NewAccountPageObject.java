package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.NewAccountPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class NewAccountPageObject extends AbstractPage{
	
	WebDriver driver;
	
	public NewAccountPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
//	public HomePageObject clickHomePage(WebDriver driver) {
//		waitForControlVisible(driver, NewCustomerPageUI.HOME_PAGE_LINK);
//		clickToElement(driver, NewCustomerPageUI.HOME_PAGE_LINK);
//		return new HomePageObject(driver);
//		
//		
//	}
	
	public boolean verifyAddNewAccountForm () {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXT, "Add new account form");
		return isControlDisplayed(driver, AbstractPageUI.DYNAMIC_TEXT, "Add new account form");
	}
	
	public void sendkeyAccountNo(String accountNo) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "cusid");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountNo, "cusid");
		
	}
	
	public void sendkeyInitialDeposit(String initialDeposit) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "inideposit");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, initialDeposit, "inideposit");
		
	}
	
	public void chooseDDLAccount(String value) {
		waitForControlVisible(driver, NewAccountPageUI.DDL_ACCOUNT);
		selectItemInDropdown(driver, NewAccountPageUI.DDL_ACCOUNT, value);
	}
	
	public void clickSubmitBtn() {
		waitForControlVisible(driver, NewAccountPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, NewAccountPageUI.CUSTOMER_SUBMIT_BTN);
		
	}
	
	public boolean verifyMessageAccountGenerated() {
		waitForControlVisible(driver, NewAccountPageUI.CUSTOMER_MESSAGE_ACCOUNT_GENERATED_SUCCESS);
		return isControlDisplayed(driver, NewAccountPageUI.CUSTOMER_MESSAGE_ACCOUNT_GENERATED_SUCCESS);
		
		
	}
	
	
	public String getAccountID() {
		waitForControlVisible(driver, NewAccountPageUI.ACCOUNT_ID);
		return getTextElement(driver, NewAccountPageUI.ACCOUNT_ID);
	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, NewAccountPageUI.CUSTOMER_CURRENT_AMOUNT);
		return getTextElement(driver, NewAccountPageUI.CUSTOMER_CURRENT_AMOUNT);
	}


}
