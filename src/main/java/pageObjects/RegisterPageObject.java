package pageObjects;

import interfaces.RegisterPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class RegisterPageObject extends AbstractPage  {
	
	WebDriver driver;
	
	public RegisterPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public void inputEmail(String email) {
		waitForControlVisible(driver, RegisterPageUI.EMAIL_TEXTBOX);
		sendkeyToElement(driver, RegisterPageUI.EMAIL_TEXTBOX, email);
		
	}
	
	public void clickSubmitBtn() {
		waitForControlVisible(driver, RegisterPageUI.SUBMIT_BTN);
		clickToElement(driver, RegisterPageUI.SUBMIT_BTN);
		
	}
	
	public String getUserID() {
		waitForControlVisible(driver, RegisterPageUI.GET_USERID);
		return getTextElement(driver, RegisterPageUI.GET_USERID);
		
	}
	public String getPassword() {
		waitForControlVisible(driver, RegisterPageUI.GET_PASSWORD);
		return getTextElement(driver, RegisterPageUI.GET_PASSWORD);
		
	}
	
	public LoginPageObject openLoginPage(String loginURL) {
		openAnyURL(driver, loginURL);
		return PageManagerDriver.getLoginPage(driver);
		
	}

}
