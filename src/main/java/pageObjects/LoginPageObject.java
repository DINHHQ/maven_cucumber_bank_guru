package pageObjects;

import interfaces.LoginPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class LoginPageObject extends AbstractPage {
	
	WebDriver driver;
	
	public LoginPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public String getURLLogIn() {
		return getCurrentURL(driver);
	} 
	
	public RegisterPageObject clickHereLink() {
		waitForControlVisible(driver, LoginPageUI.HERE_LINK);
		clickToElement(driver, LoginPageUI.HERE_LINK);
		return PageManagerDriver.getRegisterPage(driver);
		
	}
	
	public void inputUserid(String UserId) {
		waitForControlVisible(driver, LoginPageUI.USERID_TEXTBOX);
		sendkeyToElement(driver, LoginPageUI.USERID_TEXTBOX, UserId);
		
	}
	
	public void inputPassWord(String passWord) {
		waitForControlVisible(driver, LoginPageUI.PASSWORD_TEXTBOX);
		sendkeyToElement(driver, LoginPageUI.PASSWORD_TEXTBOX, passWord);
		
	}
	
	public HomePageObject clickLoginBtn(){
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		return PageManagerDriver.gethomePageObject(driver);
		
	}
	

}
