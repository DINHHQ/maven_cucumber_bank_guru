package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.DepositPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class DepositPageObject extends AbstractPage{
	
	WebDriver driver;
	
	public DepositPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
//	public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
//		waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
//		clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
//		return new NewAccountPageObject(driver);
//		
//	}
	
	public void sendkeyAccountNo(String accountNo) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "accountno");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountNo, "accountno");
		
	}
	
	public void sendkeyAmount(String amount) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "ammount");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, amount, "ammount");
		
	}
	
	public void sendkeyDescription(String description) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "desc");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, description, "desc");
		
	}
	
	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		
	}

	public boolean verifyMessageSuccessDeposit(String accountno) {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_MESSAGE_DEPOSIT_SUCCESS,accountno);
		return isControlDisplayed(driver, DepositPageUI.CUSTOMER_MESSAGE_DEPOSIT_SUCCESS,accountno);
		
		
	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_CURRENT_AMOUNT);
		return getTextElement(driver, DepositPageUI.CUSTOMER_CURRENT_AMOUNT);
	}



}
