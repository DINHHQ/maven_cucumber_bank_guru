package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.NewCustomerPageUI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import commons.AbstractPage;

public class NewCustomerPageObject extends AbstractPage {

	WebDriver driver;


	public NewCustomerPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}

	// public EditCustomerPageObject clickEditCustomerLink(WebDriver driver) {
	// waitForControlVisible(driver, NewCustomerPageUI.EDIT_CUSTOMER_LINK);
	// clickToElement(driver, NewCustomerPageUI.EDIT_CUSTOMER_LINK);
	// return new EditCustomerPageObject(driver);
	//
	// }

	public boolean verifyAddNewCustomer() {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXT, "Add New Customer");
		return isControlDisplayed(driver, AbstractPageUI.DYNAMIC_TEXT, "Add New Customer");
	}

	public void sendKeyDynamic(String customerName, String locator) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, locator);
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, customerName, locator);
	}

	public void sendKeyDateOfBirth(String dateOfBirth) {

		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "dob");
		WebElement element = driver.findElement(By.xpath(NewCustomerPageUI.CUSTOMER_DATE_OF_DATE));
		removeAttributeInDOM(driver, element, "type");
		sendkeyToElementDate(driver, AbstractPageUI.CUSTOMER_DYNAMIC, dateOfBirth, "dob");
	}

	public void sendKeyAddr(String address) {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_ADDR);
		sendkeyToElement(driver, NewCustomerPageUI.CUSTOMER_ADDR, address);
	}

	public void sendkeyCity(String city) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "city");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, city, "city");
	}

	public void sendkeyState(String state) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "state");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, state, "state");
	}

	public void sendkeyPIN(String PIN) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "pinno");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, PIN, "pinno");
	}

	public void sendkeyPhone(String telephoneno) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "telephoneno");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, telephoneno, "telephoneno");
	}

	public void sendkeyEmail(String email) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "emailid");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, email, "emailid");
	}

	public void sendkeyPass(String password) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "password");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, password, "password");
	}

	public void checkGender() {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_GENDER);
		checkTheCheckbox(driver, NewCustomerPageUI.CUSTOMER_GENDER);

	}
	
	public void clickSubmitBtn() {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, NewCustomerPageUI.CUSTOMER_SUBMIT_BTN);
		
	}
	
	public boolean verifyMessgeSuccessAddNewCustomer() {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_MESSAGE_EDIT_SUCCESS);
		return isControlDisplayed(driver, NewCustomerPageUI.CUSTOMER_MESSAGE_EDIT_SUCCESS);
	}
	
	public String getCustomerID() {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_ID);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMER_ID);
	}
	

}
