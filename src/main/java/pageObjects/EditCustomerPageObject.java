package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.EditCustomerPageUI;
import interfaces.NewCustomerPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class EditCustomerPageObject extends AbstractPage{
	
	WebDriver driver;
	
	public EditCustomerPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
//	public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
//		waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
//		clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
//		return new NewAccountPageObject(driver);
//
//	}
	
	public void inputCustomerID (String customerID) {
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMER_ID_TXT );
		sendkeyToElement(driver, EditCustomerPageUI.CUSTOMER_ID_TXT, customerID);
		
	}
	

	



}
