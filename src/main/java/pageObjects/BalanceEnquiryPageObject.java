package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.BalanceEnquiryPageUI;
import interfaces.DepositPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class BalanceEnquiryPageObject extends AbstractPage {

	WebDriver driver;

	public BalanceEnquiryPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}

	// public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
	// waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// return new NewAccountPageObject(driver);
	//
	// }

	public void sendkeyAccountNo(String accountno) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "accountno");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountno, "accountno");

	}


	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);

	}

	public boolean verifyMessageSuccessBalance(String accountno ) {
		waitForControlVisible(driver, BalanceEnquiryPageUI.CUSTOMER_MESSAGE_BALANCE_SUCCESS,accountno);
		return isControlDisplayed(driver, BalanceEnquiryPageUI.CUSTOMER_MESSAGE_BALANCE_SUCCESS,accountno);

	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, BalanceEnquiryPageUI.BALANCE);
		return getTextElement(driver, BalanceEnquiryPageUI.BALANCE);

	}
	


}
