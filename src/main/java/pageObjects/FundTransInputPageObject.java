package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.DepositPageUI;
import interfaces.FundTransInputPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class FundTransInputPageObject extends AbstractPage {

	WebDriver driver;

	public FundTransInputPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}

	// public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
	// waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// return new NewAccountPageObject(driver);
	//
	// }

	public void sendkeyPayersAccountNo(String accountPayerNo) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "payersaccount");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountPayerNo, "payersaccount");

	}

	public void sendkeyPayeesAccountNo(String accountPayerNo) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "payeeaccount");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountPayerNo, "payeeaccount");

	}

	public void sendkeyAmount(String ammount) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "ammount");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, ammount, "ammount");

	}

	public void sendkeyDescription(String description) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "desc");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, description, "desc");

	}

	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);

	}

	public boolean verifyMessageSuccessFundTrans() {
		waitForControlVisible(driver, FundTransInputPageUI.CUSTOMER_MESSAGE_FUNDTRANS_SUCCESS);
		return isControlDisplayed(driver, FundTransInputPageUI.CUSTOMER_MESSAGE_FUNDTRANS_SUCCESS);

	}
	
	public boolean verifyAmountFundTrans() {
		waitForControlVisible(driver, FundTransInputPageUI.CUSTOMER_MESSAGE_FUNDTRANS_SUCCESS);
		return isControlDisplayed(driver, FundTransInputPageUI.CUSTOMER_MESSAGE_FUNDTRANS_SUCCESS);

	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, FundTransInputPageUI.CUSTOMER_CURRENT_AMOUNT);
		return getTextElement(driver, FundTransInputPageUI.CUSTOMER_CURRENT_AMOUNT);
	}

}
