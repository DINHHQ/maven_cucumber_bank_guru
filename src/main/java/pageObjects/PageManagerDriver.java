package pageObjects;

import org.openqa.selenium.WebDriver;

public class PageManagerDriver {
	
	private static LoginPageObject loginPage;
	private static RegisterPageObject registerPage;
	private static EditCustomerPageObject editCustomerPage;
	private static NewAccountPageObject newAccountPage;
	private static HomePageObject homePage;
	private static NewCustomerPageObject newCustomerPage;
	private static DepositPageObject depositPage;
	private static WithdrawalPageObject withdrawalPage;
	private static FundTransInputPageObject fundTransInputPage;
	private static BalanceEnquiryPageObject balanceEnquiryPage;
	private static DeleteAccountPageObject deleteAccountPage;
	private static DeleteCustomerPageObject deleteCustomerPage;
	private static AbstactPageObjects abstractPage;
	
	public static LoginPageObject getLoginPage(WebDriver driver) {
		if(loginPage==null) {
			return new LoginPageObject(driver);
		}
		return loginPage;
		
	}
	
	public static RegisterPageObject getRegisterPage(WebDriver driver) {
		if(registerPage==null) {
			return new RegisterPageObject(driver);
		}
		return registerPage;
	}
	
	public static NewCustomerPageObject getNewCustomerPageObject(WebDriver driver) {
		if(newCustomerPage==null) {
			return new NewCustomerPageObject(driver);
		}
		return newCustomerPage;
	}
	
	public static EditCustomerPageObject getEditCustomerPageObject(WebDriver driver) {
		if(editCustomerPage==null) {
			return  new EditCustomerPageObject(driver);
		}
		return editCustomerPage;
	}
	
	public static NewAccountPageObject getNewAccountPage(WebDriver driver) {
		if(newAccountPage==null) {
			return new NewAccountPageObject(driver);
		}
		return newAccountPage;
	}
	
	public static HomePageObject gethomePageObject(WebDriver driver) {
		if(homePage==null) {
			return new HomePageObject(driver);
		}
		return homePage;
	}
	
	public static DepositPageObject getDepositPageObject(WebDriver driver) {
		if(depositPage==null) {
			return new DepositPageObject(driver);
		}
		return depositPage;
	}
	
	public static WithdrawalPageObject getWithdrawalPageObject(WebDriver driver) {
		if(withdrawalPage==null) {
			return new WithdrawalPageObject(driver);
		}
		return withdrawalPage;
	}
	
	public static FundTransInputPageObject getFundTransInputPageObject(WebDriver driver) {
		if(fundTransInputPage==null) {
			return new FundTransInputPageObject(driver);
		}
		return fundTransInputPage;
	}
	
	public static BalanceEnquiryPageObject getBalanceEnquiryPageObject(WebDriver driver) {
		if(balanceEnquiryPage==null) {
			return new BalanceEnquiryPageObject(driver);
		}
		return balanceEnquiryPage;
	}
	
	public static DeleteAccountPageObject getDeleteAccountPageObject(WebDriver driver) {
		if(deleteAccountPage==null) {
			return new DeleteAccountPageObject(driver);
		}
		return deleteAccountPage;
	}
	
	public static DeleteCustomerPageObject getDeleteCustomerPageObject(WebDriver driver) {
		if(deleteCustomerPage==null) {
			return new DeleteCustomerPageObject(driver);
		}
		return deleteCustomerPage;
	}
	
	public static AbstactPageObjects getAbstactPageObjects(WebDriver driver) {
		if(abstractPage==null) {
			return new AbstactPageObjects(driver);
		} 
		return abstractPage;
	}
}
