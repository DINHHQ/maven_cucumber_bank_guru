package pageObjects;

import interfaces.HomePageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class HomePageObject extends AbstractPage {
	WebDriver driver;
	
	public HomePageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public boolean verifyWelcomeMessageDisplayed() {
		waitForControlVisible(driver, HomePageUI.LABEL_RESULT);
		return isControlDisplayed(driver, HomePageUI.LABEL_RESULT);
		
	}
	
//	public NewCustomerPageObject clickNewCustomerLink (WebDriver driver) {
//		waitForControlVisible(driver, HomePageUI.NEW_CUSTOMER_LINK);
//		clickToElement(driver, HomePageUI.NEW_CUSTOMER_LINK);
//		return new NewCustomerPageObject(driver);
//	}

}
