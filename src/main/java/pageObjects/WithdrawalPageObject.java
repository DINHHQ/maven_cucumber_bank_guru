package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.DepositPageUI;
import interfaces.WithdrawalPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class WithdrawalPageObject extends AbstractPage{
	
	WebDriver driver;
	
	public WithdrawalPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
//	public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
//		waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
//		clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
//		return new NewAccountPageObject(driver);
//		
//	}
	
	public void sendkeyAccountNo(String accountNo) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "accountno");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountNo, "accountno");
		
	}
	
	public void sendkeyAmount(String amount) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "ammount");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, amount, "ammount");
		
	}
	
	public void sendkeyDescription(String description) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "desc");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, description, "desc");
		
	}
	
	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		
	}

	public boolean verifyMessageSuccessWithdraw(String accountno) {
		waitForControlVisible(driver, WithdrawalPageUI.CUSTOMER_MESSAGE_WITHDRAWAL_SUCCESS,accountno);
		return isControlDisplayed(driver, WithdrawalPageUI.CUSTOMER_MESSAGE_WITHDRAWAL_SUCCESS,accountno);
		
		
	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, WithdrawalPageUI.CUSTOMER_CURRENT_AMOUNT);
		return getTextElement(driver, WithdrawalPageUI.CUSTOMER_CURRENT_AMOUNT);
	}



}
