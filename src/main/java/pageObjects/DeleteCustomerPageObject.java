package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.BalanceEnquiryPageUI;
import interfaces.DepositPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class DeleteCustomerPageObject extends AbstractPage {

	WebDriver driver;

	public DeleteCustomerPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}

	// public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
	// waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// return new NewAccountPageObject(driver);
	//
	// }

	public void sendkeyCustomerNo(String cusid) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "cusid");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, cusid, "cusid");

	}


	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);

	}

	public boolean verifyMessageSuccessBalance() {
		waitForControlVisible(driver, BalanceEnquiryPageUI.CUSTOMER_MESSAGE_BALANCE_SUCCESS);
		return isControlDisplayed(driver, BalanceEnquiryPageUI.CUSTOMER_MESSAGE_BALANCE_SUCCESS);

	}
	
	public boolean verifyBalance() {
		waitForControlVisible(driver, BalanceEnquiryPageUI.BALANCE);
		return isControlDisplayed(driver, BalanceEnquiryPageUI.BALANCE);

	}
	


}
