package pageObjects;

import interfaces.AbstractPageUI;
import interfaces.DepositPageUI;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;

public class DeleteAccountPageObject extends AbstractPage {

	WebDriver driver;

	public DeleteAccountPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}

	// public NewAccountPageObject clickNewAccountLink(WebDriver driver) {
	// waitForControlVisible(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// clickToElement(driver, EditCustomerPageUI.NEW_ACCOUNT_LINK);
	// return new NewAccountPageObject(driver);
	//
	// }

	public void sendkeyAccountNo(String accountno) {
		waitForControlVisible(driver, AbstractPageUI.CUSTOMER_DYNAMIC, "accountno");
		sendkeyToElement(driver, AbstractPageUI.CUSTOMER_DYNAMIC, accountno, "accountno");

	}


	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.CUSTOMER_SUBMIT_BTN);

	}


	


}
