package pageObjects;

import interfaces.AbstractPageUI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import commons.AbstractPage;

public class AbstactPageObjects extends AbstractPage {
	
	WebDriver driver;

	
	public AbstactPageObjects(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public void inputToDynamicTextBox( String textboxAddr, String dataTest ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTBOX, textboxAddr);
		sendkeyToElement(driver, AbstractPageUI.DYNAMIC_TEXTBOX, dataTest, textboxAddr);
		
	}
	
	public void inputToDynamicDateBox( String textboxAddr, String dataTest ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTBOX, textboxAddr);
		WebElement datebox = driver.findElement(By.xpath("//input[@name='dob']"));
		removeAttributeInDOM(driver, datebox, "type");
		sendkeyToElement(driver, AbstractPageUI.DYNAMIC_TEXTBOX, dataTest, textboxAddr);
		
	}
	
	public void clickToDynamicButton( String buttonAddr ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_BUTTON, buttonAddr);
		clickToElement(driver, AbstractPageUI.DYNAMIC_BUTTON, buttonAddr);
		
	}
	
	public void inputToDynamicTextArea( String textboxAreaAddr, String dataTest ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTAREA, textboxAreaAddr);
		sendkeyToElement(driver, AbstractPageUI.DYNAMIC_TEXTAREA, dataTest, textboxAreaAddr);
		
	}
	
	public void clickToDynamicRadioButton( String radioButtonAddr ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_RADIO_BUTTON, radioButtonAddr);
		clickToElement(driver, AbstractPageUI.DYNAMIC_RADIO_BUTTON, radioButtonAddr);
		
	}
	
	public void clickToDynamicLink( String linkAddr ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, linkAddr);
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, linkAddr);
		
	}
	
	public boolean isDynamicSuccessMessageDisplayed( String successMessage ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_SUCCESS_MESSAGE, successMessage);
		return isControlDisplayed(driver, AbstractPageUI.DYNAMIC_SUCCESS_MESSAGE, successMessage);
		
	}
	
	public String getDataDynamicLabelAtTable( String labelAddr ) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LABEL, labelAddr);
		return getTextElement(driver, AbstractPageUI.DYNAMIC_LABEL,labelAddr);
		
	}

}
