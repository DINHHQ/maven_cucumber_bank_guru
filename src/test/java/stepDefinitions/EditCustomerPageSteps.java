package stepDefinitions;

import java.awt.font.ShapeGraphicAttribute;
import java.util.Random;

import org.openqa.selenium.WebDriver;

import pageObjects.EditCustomerPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class EditCustomerPageSteps {

	WebDriver driver;
	private EditCustomerPageObject editCustomerPage;
	private ShareData shareData;


	public EditCustomerPageSteps(ShareData shareData) {
		driver = Hooks.openBrowser();
		this.shareData = shareData;
		editCustomerPage = PageManagerDriver.getEditCustomerPageObject(driver);
	}
	
	@When("^I input CustomerID$")
	public void iInputCustomerID() {
		editCustomerPage.inputCustomerID(shareData.getCustomer());

	}
	
	



	public int random() {
		Random ran = new Random();
		int random = ran.nextInt(9999);
		return random;

	}

}
