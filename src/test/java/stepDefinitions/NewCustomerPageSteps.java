package stepDefinitions;

import java.util.Random;

import org.openqa.selenium.WebDriver;

import pageObjects.NewCustomerPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class NewCustomerPageSteps {

	WebDriver driver;
	private NewCustomerPageObject newCustomerPage;
	private ShareData shareData;
	String dateOfBirth = "01/01/1989";
	String addRess = "PO Box 911 8331 Duis Avenue";
	String emailnew = "automation" + random() + "@gmail.com";
	//public static String customerID;

	public NewCustomerPageSteps(ShareData shareData) {
		driver = Hooks.openBrowser();
		this.shareData = shareData;
		newCustomerPage = PageManagerDriver.getNewCustomerPageObject(driver);
	}

	@When("^I input all infomation to New Customer form$")
	public void iInputAllInfomationToNewCustomerForm() {
		newCustomerPage.sendKeyDynamic("auto", "name");
		newCustomerPage.sendKeyDateOfBirth(dateOfBirth);
		newCustomerPage.sendKeyAddr(addRess);
		newCustomerPage.checkGender();
		newCustomerPage.sendKeyDynamic("Tampa", "city");
		newCustomerPage.sendKeyDynamic("FL", "state");
		newCustomerPage.sendKeyDynamic("466250", "pinno");
		newCustomerPage.sendKeyDynamic("4555442476", "telephoneno");
		newCustomerPage.sendkeyEmail(emailnew);
		newCustomerPage.sendKeyDynamic("automation", "password");

	}

	@When("^I click to Submit button$")
	public void iClickToSubmitButton() throws Exception {
		newCustomerPage.clickSubmitBtn();
		Thread.sleep(5000);

	}
	
	@Then("^I get CustomerID$")
	public void iGetCustomerID( ) {
	//shareData.customerID = newCustomerPage.getCustomerID();
	shareData.setCustomer(newCustomerPage.getCustomerID());


	}

	public int random() {
		Random ran = new Random();
		int random = ran.nextInt(9999);
		return random;

	}

}
