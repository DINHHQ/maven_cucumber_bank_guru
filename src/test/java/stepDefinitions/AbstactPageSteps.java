package stepDefinitions;

import org.openqa.selenium.WebDriver;

import pageObjects.AbstactPageObjects;
import pageObjects.EditCustomerPageObject;
import pageObjects.HomePageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.PageManagerDriver;

import commons.AbstractPage;
import commons.AbstractTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class AbstactPageSteps extends AbstractPage {
	
	WebDriver driver;
	private AbstactPageObjects abstractPage;
	private AbstractTest abstractTest;
	private NewCustomerPageObject newCustomerPage;
	private EditCustomerPageObject editCustomerPage;
	private HomePageObject homePage;
	
	
	public AbstactPageSteps() {
		driver = Hooks.openBrowser();
		abstractPage = PageManagerDriver.getAbstactPageObjects(driver);
		abstractTest = new AbstractTest();
		
	}
	
	@When("^I input to \"(.*?)\" textbox with data \"(.*?)\"$")
	public void iInputToTextboxWithData(String textboxAddr, String dataTest)   {

	    abstractPage.inputToDynamicTextBox(textboxAddr, dataTest);
	     
	}
	
	@When("^I input to \"(.*?)\" datebox with data \"(.*?)\"$")
	public void iInputToDateboxWithData(String textboxAddr, String dataTest)   {

	    abstractPage.inputToDynamicDateBox(textboxAddr, dataTest);
	     
	}

	@When("^I input to \"(.*?)\" textbox with random data \"(.*?)\"$")
	public void iInputToTextboxWithRandomData(String textboxAddr, String dataTest)   {
	    dataTest =  abstractTest.randomData()+ dataTest;
	    abstractPage.inputToDynamicTextBox(textboxAddr, dataTest);
	     
	}

	@When("^I click to \"(.*?)\" button$")
	public void iClickToButton(String buttonAddr)   {
	    abstractPage.clickToDynamicButton(buttonAddr);
	     
	}

	@When("^I input to \"(.*?)\" textarea with data \"(.*?)\"$")
	public void iInputToTextareaWithData(String textboxAreaAddr, String dataTest)   {
	    abstractPage.inputToDynamicTextArea(textboxAreaAddr, dataTest);
	     
	}

	@When("^I input to \"(.*?)\" textarea with random data \"(.*?)\"$")
	public void iInputToTextareaWithRandomData(String textboxAreaAddr, String dataTest)   {
	    dataTest = dataTest + abstractTest.randomData();
	    abstractPage.inputToDynamicTextArea(textboxAreaAddr, dataTest);
	     
	}

	@When("^I click to \"(.*?)\" radio button$")
	public void iClickToRadioButton(String radioButtonAddr)   {
	    abstractPage.clickToDynamicRadioButton(radioButtonAddr);
	     
	}
	
	@Then("^I verify suscess message displayed with \"(.*?)\"$")
	public void iVerifySuscessMessageDisplayedWith(String successMessage)  {
		abstractTest.verifyTrue(abstractPage.isDynamicSuccessMessageDisplayed(successMessage));
		
		
	}
	
	@Then("^I verify expected data at \"(.*?)\" label with actual data \"(.*?)\"$")
	public void iVerifyExpectedDataAtLabelWithActualData(String labelName, String actualData)  {
		abstractTest.verifyEquals(actualData, abstractPage.getDataDynamicLabelAtTable(labelName));

	}

	@Then("^I verify expected data at \"(.*?)\" field with actual data \"(.*?)\"$")
	public void iVerifyExpectedDataAtFieldWithActualData(String arg1, String arg2)  {

	}

	@When("^I open \"(.*?)\" page$")
	public void iOpenPage(String pageName)   {
		switch (pageName) {
		case "New Customer":
			newCustomerPage = abstractPage.openNewCustomerPage(driver);
			break;
		case "Edit Customer":
			editCustomerPage = abstractPage.openEditCustomerPage(driver);
			break;

		default:
			homePage = PageManagerDriver.gethomePageObject(driver);
			break;
		}
	    
	     
	}
	


}
