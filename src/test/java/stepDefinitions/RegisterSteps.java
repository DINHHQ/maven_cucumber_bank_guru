//package stepDefinitions;
//
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.WebDriverWait;
//
//import cucumber.api.DataTable;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.When;
//
//public class RegisterSteps {
//	
//	WebDriver driver;
//	WebDriverWait wait;
//
//	@Given("^I open My Account page$")
//	public void iOpenMyAccountPage() {
//		System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
//		driver = new ChromeDriver();
//		wait = new WebDriverWait(driver, 30);
//		driver.get("http://live.guru99.com/index.php/customer/account/login/");
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//	}
//
//	@Given("^I input email \"(.*?)\" and password \"(.*?)\"$")
//	public void iInputEmailAndPassword(String username, String password) {
//		
//		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(username);
//		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(password);
//		
//	}
//
//	@When("^I input email and password$")
//	public void iInputEmailAndPassword( DataTable table) {
//		List<Map<String,String>> customer = table.asMaps(String.class, String.class);
//		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(customer.get(0).get("User"));
//		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(customer.get(0).get("Password"));
//
//	}
//	
//	@When("^I input email (.*?) vs password (.*?)$")
//	public void iInputEmailPassword(String username, String password)  {
//		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(username);
//		driver.findElement(By.xpath("//input[@id='pass']")).clear();
//		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(password);
//		System.out.println(password);
//
//	}
//	
//	@When("^I click LOGIN button$")
//	public void iClickLOGINButton() {
//		driver.findElement(By.xpath("//button[@id='send2']")).click();
//
//	}
//	
//	
//	@When("^I quit Browser$")
//	public void iQuitBrowser()  {
//	   driver.close();
//	    
//	}
//	
//}