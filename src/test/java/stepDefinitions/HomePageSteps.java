package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pageObjects.HomePageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class HomePageSteps {

	WebDriver driver;
	private HomePageObject homePageObject;

	public HomePageSteps() {
		driver = Hooks.openBrowser();
		homePageObject = PageManagerDriver.gethomePageObject(driver);

	}
	
	@Then("^I verify HomePage displayed$")
	public void iVeryfiHomePageDisplayed() {
		Assert.assertTrue(homePageObject.verifyWelcomeMessageDisplayed());

	}

	@When("^I click to New Customer page$")
	public void iClickToNewCustomerPage()  {
		homePageObject.openNewCustomerPage(driver);

	}

	@When("^I click to Edit Customer page$")
	public void iClickToEditCustomerPage()  {
		homePageObject.openEditCustomerPage(driver);

	}

}
