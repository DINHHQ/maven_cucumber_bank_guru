package stepDefinitions;

import org.openqa.selenium.WebDriver;

import pageObjects.LoginPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.Given;
import cucumberConfig.Hooks;

public class LoginPageSteps {
	WebDriver driver;
	private LoginPageObject loginPageObject;

	public LoginPageSteps() {
		driver = Hooks.openBrowser();
		loginPageObject = PageManagerDriver.getLoginPage(driver);
	}

	@Given("^I input user name \"(.*?)\"$")
	public void iInputUserName(String user) {
		loginPageObject.inputUserid(user);

	}

	@Given("^I input password \"(.*?)\"$")
	public void iInputPassword(String password) {
		loginPageObject.inputPassWord(password);

	}

	@Given("^I click Login button$")
	public void iClickLoginButton() {
		loginPageObject.clickLoginBtn();

	}



}
