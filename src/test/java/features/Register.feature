@dataDriven
Feature: Register - Demo for DataDriven in CuCumber ( Feature layer )
  I want to use this template for my feature file

  Scenario: Register 01
    Given I open My Account page
    #01
    And I input email "automationvalid@gmail.com" and password "111111"
    And I click LOGIN button
    When I quit Browser

  Scenario: Register 02
    Given I open My Account page
    #02
    When I input email and password
      | User                      | Password |
      | automationvalid@gmail.com |   111111 |
    And I click LOGIN button
    And I quit Browser

  Scenario Outline: Register 03
    Given I open My Account page
    #03
    And I input email <User> vs password <Password>
    And I click LOGIN button
    When I quit Browser

    Examples: 
      | User                      | Password |
      | automationvalid@gmail.com |   111111 |
