@payment
Feature: PAYMENT FEATURE
  As an Automation Tester
  I want to check payment process
  So that I can make sure this function work well

  Scenario Outline: [PRE-DATA] - REGISTER/ LOGIN
    Given I input to "uid" textbox with data "mngr153121"
    When I input to "password" textbox with data "pEdypAm"
    And I click to "btnLogin" button
    Then I verify HomePage displayed
    When I open "New Customer" page
    When I input to "name" textbox with data "<Name>"
    When I click to "m" radio button
    When I input to "dob" datebox with data "<Date of Birth>"
    When I input to "addr" textarea with data "<Address>"
    When I input to "city" textbox with data "<City>"
    When I input to "state" textbox with data "<State>"
    When I input to "pinno" textbox with data "<Pin>"
    When I input to "telephoneno" textbox with data "<Phone>"
    When I input to "emailid" textbox with random data "<Email>"
    When I input to "password" textbox with data "<PassWord>"
    When I click to "sub" button
    And I get CustomerID
    Then I verify suscess message displayed with "Customer Registered Successfully!!!"
    And I verify expected data at "Customer Name" label with actual data "<Name>"
    And I verify expected data at "Gender" label with actual data "male"
    And I verify expected data at "Address" label with actual data "<Address>"
    And I verify expected data at "City" label with actual data "<City>"
    And I verify expected data at "State" label with actual data "<State>"
    And I verify expected data at "Pin" label with actual data "<Pin>"

    # And I verify expected data at "" field with actual data ""
    Examples: 
      | Name      | Date of Birth | Address     | City | State       | Pin    | Phone      | Email      | PassWord    |
      | Dinh Hang | 12/22/1992    | W2 Solution | HCM  | HO CHI MINH | 111111 | 0935856471 | @gmail.com | password123 |

  Scenario: [PRE-DATA] - EDITCUSTOMER
    Given I open "Edit Customer" page
    When I input CustomerID
    When I click to "AccSubmit" button
